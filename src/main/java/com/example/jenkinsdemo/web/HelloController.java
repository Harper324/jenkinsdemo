package com.example.jenkinsdemo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {

    @GetMapping("/hello")
    public String getHello() {
        return "Hello World!";
    }
    @GetMapping("/hi")
    public String getHi() {
        return "Hi";
    }

    @GetMapping("/winter")
    public String getWinter() {
        return "Winter is coming!";
    }
}
