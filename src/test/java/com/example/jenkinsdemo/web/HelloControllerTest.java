package com.example.jenkinsdemo.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class HelloControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_hello_world() throws Exception {

        mockMvc.perform(get("/api/hello"))
                .andExpect(status().is(200))
                .andExpect(content().string("Hello World!"));
    }

    @Test
    void should_return_hi() throws Exception {

        mockMvc.perform(get("/api/hi"))
                .andExpect(status().is(200))
                .andExpect(content().string("Hi"));
    }

    @Test
    void should_return_winter() throws Exception {

        mockMvc.perform(get("/api/winter"))
                .andExpect(status().is(200))
                .andExpect(content().string("Winter is coming!"));
    }
}